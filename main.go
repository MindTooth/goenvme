package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
)

// https://tutorialedge.net/golang/parsing-json-with-golang/
// https://yourbasic.org/golang/json-example/

func main() {
	filesFolder := os.Getenv("GOENVME_PATH")

	if filesFolder == "" {
		filesFolder = "GOENVME_PATH"

	}

	filesPreFilter, err := ioutil.ReadDir(filesFolder)

	if err != nil {
		fmt.Println("Could not open the secreta folder!")
		panic(err)
	}

	// Initiate an empty slice for copying filter data
	filesPostFilter := filesPreFilter[:0]

	for _, file := range filesPreFilter {
		matched, _ := regexp.MatchString(`.json$`, file.Name())
		if matched {
			filesPostFilter = append(filesPostFilter, file)
		}
	}

	// Clear old array
	filesPreFilter = nil

	// DEBUG: Print out files
	for _, file := range filesPostFilter {
		fmt.Println(file.Name())
	}

	// DEBUG: Open an JSON-file
	jsonFile, err := os.Open(filesFolder + "/" + filesPostFilter[0].Name())

	// Thrown an panic if file is unable to be opened
	if err != nil {
		panic(err)
	}

	// Close file after opening
	defer jsonFile.Close()

	// b, err := file.Read(jsonFile)

	// fmt.Print(b)

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var result map[string]interface{}
	json.Unmarshal([]byte(byteValue), &result)

	// fmt.Println(result)

	for k, v := range result {
		fmt.Printf("Key: %s, Value: %s\n", k, v)
		os.Setenv(k, "Hello")
	}
	// dec := json.NewDecoder(bytes.NewReader(byteValue))

	// fmt.Println(result["DOMENESHOP_TOKEN"])

	// enc := json.NewEncoder(writer)
}
