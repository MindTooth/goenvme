# GoEnvMe

Small app for loading environment secrets from a folder.

## Usage

Put secrets into the `$HOME/.secrets` folder, and load them with `goem --list`.
